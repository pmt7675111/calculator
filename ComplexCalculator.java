package calculator;

import java.util.Scanner;

public class ComplexCalculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first number:");
        double num1 = scanner.nextDouble();
        System.out.println("Enter the second number:");
        double num2 = scanner.nextDouble();
                System.out.println("The result is: " + (num1 + num2));
                System.out.println("The result is: " + (num1 - num2));
                System.out.println("The result is: " + (num1 * num2));
                if (num2 == 0) {
                    System.out.println("Error: Division by zero is not allowed.");
                } else {
                    System.out.println("The result is: " + (num1 / num2));
                }
                System.out.println("The result is: " + Math.pow(num1, num2));
        }
    }
}